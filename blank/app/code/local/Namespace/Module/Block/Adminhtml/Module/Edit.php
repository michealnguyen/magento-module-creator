<?php
/**
 * Magestore
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Magestore.com license that is
 * available through the world-wide-web at this URL:
 * http://www.magestore.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Magestore
 * @package     Magestore_<Module>
 * @copyright   Copyright (c) 2012 Magestore (http://www.magestore.com/)
 * @license     http://www.magestore.com/license-agreement.html
 */

/**
 * <Fmodule> Edit Block
 * 
 * @category     Magestore
 * @package     Magestore_<Module>
 * @author      Magestore Developer
 */
class <Namespace>_<Module>_Block_Adminhtml_<Fmodule>_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        
        $this->_objectId = 'id';
        $this->_blockGroup = '<module>';
        $this->_controller = 'adminhtml_<module>';
        
        $this->_updateButton('save', 'label', Mage::helper('<module>')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('<module>')->__('Delete Item'));
        
        $this->_addButton('saveandcontinue', array(
            'label'        => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'    => 'saveAndContinueEdit()',
            'class'        => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('<module>_content') == null)
                    tinyMCE.execCommand('mceAddControl', false, '<module>_content');
                else
                    tinyMCE.execCommand('mceRemoveControl', false, '<module>_content');
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
    
    /**
     * get text to show in header when edit an item
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('<module>_data')
            && Mage::registry('<module>_data')->getId()
        ) {
            return Mage::helper('<module>')->__("Edit Item '%s'",
                                                $this->htmlEscape(Mage::registry('<module>_data')->getTitle())
            );
        }
        return Mage::helper('<module>')->__('Add Item');
    }
}